parameters = {
    "LogisticRegression":[{}],
    "GaussianNB":[{}],
    "DecisionTreeClassifier":[{}],
    "KNeighborsClassifier":[{}],
    "RandomForestClassifier":[{}]
}
#
for c in range(1,21,1):
    c = c/10
    for max_iter in range(100,550,50):
        parameters["LogisticRegression"].append(
            {
                "C": c,
                "max_iter": max_iter
            }
        )
#
for criterion in ["gini", "entropy"]:
    for splitter in ["best", "random"]:
        parameters["DecisionTreeClassifier"].append(
            {
                "criterion": criterion,
                "splitter": splitter
            }
        )
#
for n_neighbors in range(1,10,1):
    for weights in ["uniform", "distance"]:
        for algorithm in ["auto", "ball_tree", "kd_tree", "brute"]:
            parameters["KNeighborsClassifier"].append(
                {
                    "n_neighbors": n_neighbors,
                    "weights": weights,
                    "algorithm": algorithm
                }
            )            
#
for n_estimators in range(10,200,10):
    for criterion in ["gini", "entropy"]:
        parameters["RandomForestClassifier"].append(
            {
                "n_estimators": n_estimators,
                "criterion": criterion
            }
        )            

def get_parameters(model_name, config_id):
    return parameters[model_name][config_id]