#Data handling
import pandas as pd

#Visualize
import matplotlib
import matplotlib.pyplot as plt

colors = ['tan', 'gray', 'blue', 'c', 'green', 'darkkhaki']

def set_font():
    font = {'size': 56}
    matplotlib.rc('font', **font)

def plot_measure(df, measure, save_name):
    new_dfs = []
    #
    dataset_list = list(set(df.Dataset.values))
    dataset_list.sort()
    #
    models_list = list(set(df.Model.values))
    models_list.sort(reverse=False)
    #
    for dataset in dataset_list:
        new_df = []
        for model in models_list:
            f1_scores = []
            temp = df.loc[(df['Dataset'] == dataset) & (df['Model'] == model)][measure].values
            for value in temp:
                d = {
                    'DatasetModel' : '{: <26}'.format(", ".join([dataset, model])), 
                    measure : value
                }
                new_df.append(d)

        new_df = pd.DataFrame(new_df)
        new_dfs.append(new_df)
    #
    create_plots(measure, len(dataset_list), new_dfs, save_name)

def cpp_plot_measure(df, measure, save_name):
    target_dataset_list = list(set(df.Target.values))
    target_dataset_list.sort()
    #
    source_dataset_list = list(set(df.Target.values))
    source_dataset_list.sort()
    #
    models_list = list(set(df.Model.values))
    models_list.sort(reverse=False)
    #
    new_dfs = []
    #
    for target in target_dataset_list:
        for source in source_dataset_list:
            if target == source:
                continue
            new_df = []
            for model in models_list:
                f1_scores = []
                temp = df.loc[(df['Target'] == target) & (df['Source'] == source) & (df['Model'] == model)][measure].values
                for value in temp:
                    d = {
                        'DatasetModel' : '{: <45}'.format(", ".join([target, source, model])), 
                        measure : value
                    }
                    new_df.append(d)
            new_df = pd.DataFrame(new_df)
            new_dfs.append(new_df)
    #
    create_plots(measure, len(target_dataset_list)*(len(source_dataset_list)-1), new_dfs, save_name)

def cpp_target_plot_measure(df, measure, save_name):
    target_dataset_list = list(set(df.Target.values))
    target_dataset_list.sort()
    #
    source_dataset_list = list(set(df.Target.values))
    source_dataset_list.sort()
    #
    models_list = list(set(df.Model.values))
    models_list.sort(reverse=False)
    #
    new_dfs = []
    #
    for target in target_dataset_list:
        new_df = []
        for model in models_list:
            f1_scores = []
            temp = df.loc[(df['Target'] == target) & (df['Model'] == model)][measure].values
            for value in temp:
                d = {
                    'DatasetModel' : '{: <30}'.format(", ".join([target, model])), 
                    measure : value
                }
                new_df.append(d)
        new_df = pd.DataFrame(new_df)
        new_dfs.append(new_df)
    #
    create_plots(measure, len(target_dataset_list), new_dfs, save_name)


def create_plots(measure, subplot_count, new_dfs, save_name):
    set_font()
    fig, axs = plt.subplots(subplot_count, 1, figsize=(64,128), sharex=True, sharey="row", gridspec_kw={'hspace': 0.1})

    for n, new_df in enumerate(new_dfs):

        bp_dict = new_df.boxplot(
            column=measure,
            by=["DatasetModel"],
            layout=(1,1),       
            return_type='both',
            patch_artist = True,
            vert=False,
            ax=axs[n]
        )

        for row_key, (ax,row) in bp_dict.iteritems():
            yax = ax.get_yaxis()
            yax.set_ticks_position('right')
            pad = max([len(i) for i in new_df['DatasetModel'].values])
            yax.set_tick_params(pad=pad)
            xax = ax.get_xaxis()
            ax.set_title("")
            x_label = xax.get_label()
            x_label.set_visible(False)
            for i,box in enumerate(row['boxes']):
                box.set_color(colors[i%6])
                box.set_edgecolor("black")
                box.set_linewidth(5)
            for item in row['medians']:
                item.set_color('red')
                item.set_linewidth(5)
            for item in row['whiskers']:
                item.set_color("black")
                item.set_linewidth(5)
            for item in row["caps"]:
                item.set_color("black")
                item.set_linewidth(5)   

    fig.suptitle(measure)
    st = fig.suptitle(measure, fontsize="x-large")
    st.set_y(0.95)
    fig.subplots_adjust(top=0.93)
    fig.tight_layout()
    plt.tight_layout()
    if save_name is not None:
        plt.savefig(save_name+".pdf",format='pdf',bbox_inches='tight')
    plt.show()

